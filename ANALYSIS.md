# PHP FPM Example Application

This app is a basic Hello World like example application written in PHP.
This is just to demonstrate how PHP FPM application can be deployed in
kubernetes.

## About the APP

This project contains only one php file. This is just a sample HTML code.
We use nginx as a reverse proxy which routes to this file.

## Building the app

The provided `Dockerfile` can be used to build the docker image for the app.
This uses the php version 7.2 and uses the smaller alpine version. To build
the app just run
`docker build -t <docker_url>/<repo>:<tag>`

## Deploying the app

This is tested with `minikube`. Assuming minikube is installed, you can simply
run `bash test.sh` to test the app. But note that this pull the image from the
dockerhub user msvbhat. If you have made changes and would like to pull from
somewhere else, change the kubernetes.yaml manifest.

The app can however be also deployed in a proper Kubernetes cluster. And in that
case, a different service type can be used. The kubernetes.yaml manifest creates
a LoadBalancer type service. If you have a ingress object already, that maybe
used instead (The kubernetes.yaml needs to be changed accordingly for that).

## Kubernetes concepts used

1. `configMap` to add configurations for nginx
1. `Pods` which contain both php and nginx containers
1. `Deployment` object which manages deployments
1. `Service` to access this application deployes as pods
1. `Volume` to enable shared storage between nginx and php container. We use
    emptyDir volume for now. We can also use init container to download the code
    from somewhere else and put it in the volume. But to keep things simple, I have
    used emptyDir volume.
1. `pods lifecycle` to copy the php files to the volume so that they are accessible
    for nginx container.

## Possible Improvements

There are plenty of place for improvement.

1. Adding a .gitlab-ci.yml file for below things
   - Automatic building of docker images and pushing to a registry
   - Automatic testing this against the kubernetes of minikube
1. Adding SSL certificates for secure browsing (probably self signed)
1. Creating a helm chart with which it can be easily deployed to a proper
   Kubernetes cluster
1. Using init containers to copy the php files from the gitlab.
1. Accessing the app using ingress controler object instead of LoadBalancer
   type service.
