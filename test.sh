#!/usr/bin/env bash

DEPLOYMENT="php-app"
SERVICE="php-welcome"
# Assuming minikube is already installed
minikube start
# Create the kubernetes resources
kubectl create -f kubernetes.yaml
# Wait for the deployment to be ready
kubectl wait --for=condition=available deployment/${DEPLOYMENT}

# Launch the application
minikube service ${SERVICE}
