# php-nginx-kubernetes

Prototype of deploying PHP FPM app in Kubernetes

This is just to show how a PHP-FPM app can be deployed in kubernetes
with nginx to route to the php files.

For more detailed docs please refer [analysis.md](ANALYSIS.md)
